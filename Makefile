##
# This file is part of Gamma-SciDB.
# Copyright (C) 2015 The UH DBMS Research Group, all rights reserved.
# See LICENSE.txt for more copyright information.
#
# Author: Yiqun Zhang (yzhang@cs.uh.edu)
#
# This Gamma operator is proposed in the paper:
# The Gamma Operator for Big Data Summarization on an Array DBMS
# Carlos Ordonez, Yiqun Zhang, Wellington Cabrera
# Journal of Machine Learning Research (JMLR): Workshop and Conference Proceedings (BigMine 2014)
#
# Please cite the paper above if you need to use this code in your research work.
##

# This Makefile builds all the following operators that belong to this project:
# * DenseGamma
# * SparseGamma
# * DiagDenseGamma
# * DiagSparseGamma
# * GroupDiagDenseGamma

# The SciDB source must be present in order to build. It is specified
# As the SCIDB_SOURCE_DIR variable.
# Example: make SCIDB_SOURCE_DIR=/home/user/workspace/scidb_trunk
# Or you can put it in this Makefile like this:
SCIDB_SOURCE_DIR=/home/scidb/scidbtrunk

# The scidb-boost-devel package also needs to be installed:
BOOST_LOCATION=/opt/scidb/$(SCIDB_VER)/3rdparty/boost/include

CFLAGS=-pedantic -W -Wextra -Wall -Wno-strict-aliasing -Wno-long-long -Wno-unused-parameter -Wno-variadic-macros -fPIC -D__STDC_FORMAT_MACROS -Wno-system-headers -isystem -O2 -g -DNDEBUG -ggdb3  -D__STDC_LIMIT_MACROS
INC=-I. -DPROJECT_ROOT="\"$(SCIDB_SOURCE_DIR)\"" -I"$(SCIDB_SOURCE_DIR)/include" -I"$(BOOST_LOCATION)"
LIBS=-L"$(SCIDB_SOURCE_DIR)/lib" -shared -Wl,-soname,libDenseGamma.so -L. -lm

all:
	@if test ! -d "$(SCIDB_SOURCE_DIR)"; then echo  "Error. Try:\n\nmake SCIDB_SOURCE_DIR=<PATH TO SCIDB TRUNK>"; exit 1; fi 

	$(CXX) $(CFLAGS) $(INC) -o LogicalDenseGamma.cpp.o -c DenseGamma/LogicalDenseGamma.cpp
	$(CXX) $(CFLAGS) $(INC) -o PhysicalDenseGamma.cpp.o -c DenseGamma/PhysicalDenseGamma.cpp

	$(CXX) $(CFLAGS) $(INC) -o LogicalSparseGamma.cpp.o -c SparseGamma/LogicalSparseGamma.cpp
	$(CXX) $(CFLAGS) $(INC) -o PhysicalSparseGamma.cpp.o -c SparseGamma/PhysicalSparseGamma.cpp

	$(CXX) $(CFLAGS) $(INC) -o LogicalDiagDenseGamma.cpp.o -c DiagDenseGamma/LogicalDiagDenseGamma.cpp
	$(CXX) $(CFLAGS) $(INC) -o PhysicalDiagDenseGamma.cpp.o -c DiagDenseGamma/PhysicalDiagDenseGamma.cpp

	$(CXX) $(CFLAGS) $(INC) -o LogicalDiagSparseGamma.cpp.o -c DiagSparseGamma/LogicalDiagSparseGamma.cpp
	$(CXX) $(CFLAGS) $(INC) -o PhysicalDiagSparseGamma.cpp.o -c DiagSparseGamma/PhysicalDiagSparseGamma.cpp

	$(CXX) $(CFLAGS) $(INC) -o LogicalGroupDiagDenseGamma.cpp.o -c GroupDiagDenseGamma/LogicalGroupDiagDenseGamma.cpp
	$(CXX) $(CFLAGS) $(INC) -o PhysicalGroupDiagDenseGamma.cpp.o -c GroupDiagDenseGamma/PhysicalGroupDiagDenseGamma.cpp

	$(CXX) $(CFLAGS) $(INC) -o libGammaSciDB.so \
	                           LogicalDenseGamma.cpp.o \
	                           PhysicalDenseGamma.cpp.o \
	                           LogicalSparseGamma.cpp.o \
	                           PhysicalSparseGamma.cpp.o \
	                           LogicalDiagDenseGamma.cpp.o \
	                           PhysicalDiagDenseGamma.cpp.o \
	                           LogicalDiagSparseGamma.cpp.o \
	                           PhysicalDiagSparseGamma.cpp.o \
	                           LogicalGroupDiagDenseGamma.cpp.o \
	                           PhysicalGroupDiagDenseGamma.cpp.o \
	                           $(LIBS)

clean:
	rm -f *.o *.so

install:
	@if test -z "$(SCIDB_VER)"; then echo "Error: SCIDB_VER is not set. Try:\n\nmake install SCIDB_VER=<version>"; exit 1; fi
	cp *.so /opt/scidb/$(SCIDB_VER)/lib/scidb/plugins/
	iquery -aq "load_library('GammaSciDB');"

uninstall:
	iquery -aq "unload_library('GammaSciDB');"
