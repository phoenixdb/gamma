 /*
  * This file is part of Gamma-SciDB.
  * Copyright (C) 2015 The UH DBMS Research Group, all rights reserved.
  * See LICENSE.txt for more copyright information.
 
  * Author: Yiqun Zhang (yzhang@cs.uh.edu)
 
  * We have operators for both spase and dense arrays. 
  * This operator is designed for dense arrays.
 
  * This Gamma operator is proposed in the paper:
  * The Gamma Operator for Big Data Summarization on an Array DBMS
  * Carlos Ordonez, Yiqun Zhang, Wellington Cabrera 
  * Journal of Machine Learning Research (JMLR): Workshop and Conference Proceedings (BigMine 2014) 
 
  * Please cite the paper above if you need to use this code in your research work.
  */

#include <query/Operator.h>
using namespace std;

namespace scidb
{

class LogicalDenseGamma : public LogicalOperator
{
public:    

    LogicalDenseGamma(const string& logicalName, const string& alias):
        LogicalOperator(logicalName, alias)
    {
        ADD_PARAM_INPUT()
    }
    
    ArrayDesc inferSchema(vector< ArrayDesc> schemas, shared_ptr< Query> query)
    {
        ArrayDesc const& inputSchema = schemas[0];
        Dimensions inputDims = inputSchema.getDimensions();
        
        // The input array should have 2 dimensions: i and j.
        if(inputDims.size() != 2) {
            throw SYSTEM_EXCEPTION(SCIDB_SE_OPERATOR, SCIDB_LE_ILLEGAL_OPERATION)
                  << "Operator Gamma accepts an array with exactly 2 dimensions.";
        }
        
        // The input array should have only 1 attribute, and it should be in double type.
        if (inputSchema.getAttributes(true).size() != 1 ||
            inputSchema.getAttributes(true)[0].getType() != TID_DOUBLE) {
            throw SYSTEM_EXCEPTION(SCIDB_SE_OPERATOR, SCIDB_LE_ILLEGAL_OPERATION)
                  << "Operator Gamma accepts an array with one attribute of type double";
        }
        
        // The output array has only one attribute which is exactly the same as the input.
        Attributes outputAttributes;
        outputAttributes.push_back( inputSchema.getAttributes(true)[0] );
        outputAttributes = addEmptyTagAttribute(outputAttributes);
        
        DimensionDesc dimsD = inputDims[1];
        // We assume the input data set has d columns and an additional Y columns.
        Coordinate d = dimsD.getCurrEnd() - dimsD.getCurrStart();
        
        if(dimsD.getChunkInterval() < d+1) {
            throw SYSTEM_EXCEPTION(SCIDB_SE_OPERATOR, SCIDB_LE_ILLEGAL_OPERATION)
                  << "Chunk size of the column dimension must be no less than d+1.";
        }
        
        DimensionDesc i("i", 1, d+2, d+2, 0);
        DimensionDesc j("j", 1, d+2, d+2, 0);
        Dimensions outputDims;
        outputDims.push_back(i); 
        outputDims.push_back(j); 
        return ArrayDesc("DenseGamma_" + inputSchema.getName(), outputAttributes, outputDims, defaultPartitioning());
    }
};

REGISTER_LOGICAL_OPERATOR_FACTORY(LogicalDenseGamma, "DenseGamma");

} //namespace scidb
