# SparseGamma SciDB Operator

This file is part of Gamma-SciDB.  
Copyright (C) 2015 The UH DBMS Research Group, all rights reserved.  
See LICENSE.md for more copyright information.

**Author:** Yiqun Zhang *(yzhang@cs.uh.edu)*

This Gamma operator is proposed in the paper:  
>**The Gamma Operator for Big Data Summarization on an Array DBMS** [[PDF](http://www2.cs.uh.edu/~yzhang/research/gamma.pdf)]  
Carlos Ordonez, Yiqun Zhang, Wellington Cabrera  
*Journal of Machine Learning Research (JMLR): Workshop and Conference Proceedings (BigMine 2014)*

Please cite the paper above if you need to use this code in your research work.

This *SparseGamma* operator computes the Gamma matrix on sparse SciDB arrays.

* **To compile:**  
  1. Make sure *g++* and *scidb-boost-devel* package are correctly installed on your computer.  
  2. Download latest SciDB source code from [here](http://paradigm4.com/forum/viewtopic.php?f=16&t=364).  
  **Our source code is tested under SciDB 14.12**
  3. Double check the Makefile, make sure *SCIDB_SOURCE_DIR* is correct.
  4. Run  ```make```.

* **To deploy:**  
  Run ```make install```  
  If you get the error message saying "*Permission Denied.*", run ```sudo make install SCIDB_VER=14.12```  
  If you need to remove the operator from SciDB, run ```make uninstall```.
