##
# This file is part of Gamma-SciDB.
# Copyright (C) 2015 The UH DBMS Research Group, all rights reserved.
# See LICENSE.md for more copyright information.
#
# Author: Yiqun Zhang (yzhang@cs.uh.edu)
#
# This Gamma operator is proposed in the paper:
# The Gamma Operator for Big Data Summarization on an Array DBMS
# Carlos Ordonez, Yiqun Zhang, Wellington Cabrera
# Journal of Machine Learning Research (JMLR): Workshop and Conference Proceedings (BigMine 2014)
#
# Please cite the paper above if you need to use this code in your research work.
##

# The SciDB source must be present in order to build. It is specified
# As the SCIDB_SOURCE_DIR variable.
# Example: make SCIDB_SOURCE_DIR=/home/scidb/scidbtrunk
# Or you can put it in this Makefile like this:
SCIDB_SOURCE_DIR=/home/scidb/scidbtrunk

# The scidb-boost-devel package also needs to be installed:
BOOST_LOCATION=/opt/scidb/$(SCIDB_VER)/3rdparty/boost/include

OP_NAME=DiagDenseGamma

CFLAGS=-std=c++11 -pedantic -W -Wextra -Wall -Wno-strict-aliasing -Wno-long-long -Wno-unused-parameter -Wno-variadic-macros -fPIC -D__STDC_FORMAT_MACROS -Wno-system-headers -isystem -O3 -g -DNDEBUG -ggdb3  -D__STDC_LIMIT_MACROS
INC=-I. -DPROJECT_ROOT="\"$(SCIDB_SOURCE_DIR)\"" -isystem"$(SCIDB_SOURCE_DIR)/include" -isystem"$(BOOST_LOCATION)"
LIBS=-L"$(SCIDB_SOURCE_DIR)/lib" -shared -Wl,-soname,lib$(OP_NAME).so -L. -lm

all:
	@if test ! -d "$(SCIDB_SOURCE_DIR)"; then echo  "Error. Try:\n\nmake SCIDB_SOURCE_DIR=<PATH TO SCIDB TRUNK>"; exit 1; fi 

	$(CXX) $(CFLAGS) $(INC) -o Logical$(OP_NAME).cpp.o -c Logical$(OP_NAME).cpp
	$(CXX) $(CFLAGS) $(INC) -o Physical$(OP_NAME).cpp.o -c Physical$(OP_NAME).cpp

	$(CXX) $(CFLAGS) $(INC) -o lib$(OP_NAME).so \
	                           Logical$(OP_NAME).cpp.o \
	                           Physical$(OP_NAME).cpp.o \
	                           $(LIBS)

clean:
	rm -f *.o *.so

install:
	@if test -z "$(SCIDB_VER)"; then echo "Error: SCIDB_VER is not set. Try:\n\nmake install SCIDB_VER=<version>"; exit 1; fi
	cp *.so /opt/scidb/$(SCIDB_VER)/lib/scidb/plugins/
	iquery -aq "load_library('$(OP_NAME)');"

uninstall:
	iquery -aq "unload_library('$(OP_NAME)');"
	rm -f /opt/scidb/$(SCIDB_VER)/lib/scidb/plugins/lib$(OP_NAME).so
